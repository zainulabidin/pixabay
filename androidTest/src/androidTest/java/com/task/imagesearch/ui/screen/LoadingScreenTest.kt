package com.task.imagesearch.ui.screen

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.task.androidtest.R
import com.task.imagesearch.presentation.screens.LoadingScreen
import org.junit.Rule
import org.junit.Test

class LoadingScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testLoadingDisplayed() {
        lateinit var processTag: String

        composeTestRule.setContent {
            LoadingScreen()

            processTag = stringResource(id = R.string.test_progress_tag)
        }

        composeTestRule
            .onNodeWithTag(processTag)
            .assertIsDisplayed()
    }
}
