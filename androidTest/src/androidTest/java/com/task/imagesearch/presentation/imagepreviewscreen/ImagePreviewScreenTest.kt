package com.task.imagesearch.presentation.imagepreviewscreen

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createComposeRule
import com.task.imagesearch.domain.model.PixabayResponse
import org.junit.Rule
import org.junit.Test

class ImagePreviewScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun imagePreviewScreen_contentDisplayedCorrectly() {
        val sampleImage = PixabayResponse.PixabayImage(
            id = 2310212,
            tags = "fruit, lemon, art",
            previewURL = "https://cdn.pixabay.com/photo/2017/05/13/17/31/fruit-2310212_150.jpg",
            largeImageURL = "https://pixabay.com/get/g1af53fe4006051a0ad74edcaa5eb5b7df21c493b1c04b0c1f9f3278d19eea19668edbd9762c5db61c08af24239abab060d2468f38e6ad023437640f8bba99a3f_1280.jpg",
            downloads = 100,
            likes = 42,
            comments = 8,
            user = "Yousz",
            query = "lemon"
        )

        composeTestRule.setContent {
            ImagePreviewScreen(pixabayImage = sampleImage)
        }

        // Assert the presence of the expected content
        composeTestRule.onNode(hasText("Image Preview")).assertIsDisplayed()
        composeTestRule.onNode(hasText("Yousz")).assertIsDisplayed()
        composeTestRule.onNode(hasText("fruit, lemon, art")).assertIsDisplayed()
        composeTestRule.onNode(hasText("42 Likes")).assertIsDisplayed()
        composeTestRule.onNode(hasText("100 Downloads")).assertIsDisplayed()
        composeTestRule.onNode(hasText("8 Comments")).assertIsDisplayed()
    }
}

