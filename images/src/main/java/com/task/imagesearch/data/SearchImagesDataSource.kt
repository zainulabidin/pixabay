package com.task.imagesearch.data

import com.task.imagesearch.domain.model.PixabayResponse
import kotlinx.coroutines.flow.Flow

interface SearchImagesDataSource {

    interface Remote {
        suspend fun getImagesApiResponse(query: String): PixabayResponse
    }

    interface Local {
        fun getImages(query: String): Flow<List<PixabayResponse.PixabayImage>>

        suspend fun insertImages(results: List<PixabayResponse.PixabayImage>)
    }
}
