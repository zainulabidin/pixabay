package com.task.imagesearch.data.local

import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.data.local.dao.ImagesDao
import com.task.imagesearch.domain.model.PixabayResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ImagesLocalDataSource @Inject constructor(
    private val dao: ImagesDao
) : SearchImagesDataSource.Local {

    override fun getImages(query: String): Flow<List<PixabayResponse.PixabayImage>> {
        return dao.getImages(query)
    }

    override suspend fun insertImages(results: List<PixabayResponse.PixabayImage>) {
        dao.insertImages(results)
    }

}