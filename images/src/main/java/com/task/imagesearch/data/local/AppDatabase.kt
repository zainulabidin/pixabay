package com.task.imagesearch.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.task.imagesearch.data.local.dao.ImagesDao
import com.task.imagesearch.domain.model.PixabayResponse

@Database(entities = [PixabayResponse.PixabayImage::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val imagesDao: ImagesDao
}