package com.task.imagesearch.data.repository

import com.task.core.ui.RequestStates
import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.util.NetworkHelper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SearchImageRepository @Inject constructor(
    private val remoteDataSource: SearchImagesDataSource.Remote,
    private val localDataSource: SearchImagesDataSource.Local,
    private val networkHelper: NetworkHelper
) {

    fun getImages(query: String): Flow<RequestStates<List<PixabayResponse.PixabayImage>>> {
        return flow {
            emit(RequestStates.Loading)

            val localResults = localDataSource.getImages(query).first()
            emit(RequestStates.Success(localResults))
            if (networkHelper.isNetworkConnected()) {
                try {
                    val remoteResults = remoteDataSource.getImagesApiResponse(query = query)
                    localDataSource.insertImages(remoteResults.hits.map { it.copy(query = query) })

                    val updatedLocalResults = localDataSource.getImages(query).first()
                    emit(RequestStates.Success(updatedLocalResults))
                } catch (e: Exception) {
                    emit(RequestStates.Error(e))
                }
            }
        }
    }
}





