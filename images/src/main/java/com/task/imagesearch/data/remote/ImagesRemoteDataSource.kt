package com.task.imagesearch.data.remote

import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.data.remote.retrofit.Endpoints
import com.task.imagesearch.domain.model.PixabayResponse
import javax.inject.Inject

class ImagesRemoteDataSource @Inject constructor(
    private val endpoints: Endpoints,
) : SearchImagesDataSource.Remote {

    override suspend fun getImagesApiResponse(query: String): PixabayResponse {
        return endpoints.getPixabayResponse(query = query)
    }

}