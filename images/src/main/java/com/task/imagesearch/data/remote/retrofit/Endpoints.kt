package com.task.imagesearch.data.remote.retrofit

import com.task.imagesearch.BuildConfig
import com.task.imagesearch.domain.model.PixabayResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface Endpoints {

    companion object {
        const val API_KEY = BuildConfig.API_KEY
    }

    @GET("api/")
    suspend fun getPixabayResponse(
        @Query("key") apiKey:String =  API_KEY,
        @Query("q") query: String,
    ): PixabayResponse

}