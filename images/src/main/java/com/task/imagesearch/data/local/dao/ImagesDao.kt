package com.task.imagesearch.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.task.imagesearch.domain.model.PixabayResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface ImagesDao {
    @Query("SELECT * FROM images WHERE `query`=:query")
    fun getImages(query: String): Flow<List<PixabayResponse.PixabayImage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImages(results: List<PixabayResponse.PixabayImage>)

}