package com.task.imagesearch.presentation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.imagesearch.presentation.imagepreviewscreen.ImagePreviewScreen
import com.task.imagesearch.presentation.navigation.Screen
import com.task.imagesearch.presentation.searchimagescreen.ImagesListScreen

@Composable
fun ImageSearchApp() {
    val navigation = rememberNavController()

    NavHost(
        navController = navigation,
        startDestination = Screen.ImagesListScreen.route
    ) {
        composable(route = Screen.ImagesListScreen.route) {
            val viewModel = hiltViewModel<SearchImagesViewModel>()

            ImagesListScreen(viewModel, navigation)
        }

        composable(route = Screen.ImagePreviewScreen.route) {
            val pixabayImage = navigation.previousBackStackEntry?.savedStateHandle?.get<PixabayResponse.PixabayImage>("image")
            ImagePreviewScreen(pixabayImage)
        }
    }
}
