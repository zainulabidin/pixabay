package com.task.imagesearch.presentation.searchimagescreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.task.core.ui.RequestStates
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.imagesearch.presentation.SearchImagesViewModel
import com.task.imagesearch.presentation.components.AlertDialogCompose
import com.task.imagesearch.presentation.components.SearchableToolbar
import com.task.imagesearch.presentation.navigation.Screen


@Composable
fun ImagesListScreen(
    viewModel: SearchImagesViewModel,
    navController: NavHostController
) {
    var query by remember { mutableStateOf("fruits") }
    var showDialog by remember { mutableStateOf(false) }
    var clickedImage by remember { mutableStateOf<PixabayResponse.PixabayImage?>(null) }


    LaunchedEffect(Unit) {
        viewModel.getImages(query)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {

        SearchableToolbar(onQueryUpdated = { updatedQuery ->
            query = updatedQuery
            viewModel.getImages(updatedQuery)
        }, query = query)

        val viewStates by viewModel.imagesState.collectAsState()
        when (viewStates) {
            is RequestStates.Loading -> {
                Text(
                    "Loading...",
                    Modifier.padding(16.dp)
                )
            }

            is RequestStates.Success -> {
                val images =
                    (viewStates as RequestStates.Success<List<PixabayResponse.PixabayImage>>).data
                ImageList(images = images) {
                    clickedImage = it
                    showDialog = true
                }

            }

            is RequestStates.Error -> {
                val error = (viewStates as RequestStates.Error).exception
                Text(
                    "${error.message}",
                    Modifier.padding(16.dp)
                )
            }

        }

        if (showDialog) {
            AlertDialogCompose(
                title = "Are you sure?",
                message = "Do you want to view more details?",
                onConfirm = {
                    navController.currentBackStackEntry?.savedStateHandle?.set(
                        "image",
                        clickedImage
                    )
                    navController.navigate(Screen.ImagePreviewScreen.route)
                },
                onClose = { showDialog = false }
            )
        }
    }
}

@Composable
fun ImageList(
    images: List<PixabayResponse.PixabayImage>,
    onItemClick: (PixabayResponse.PixabayImage) -> Unit
) {
    LazyColumn(modifier = Modifier.padding(12.dp)) {
        items(images) { image ->
            ImageListItem(image = image, onItemClick = onItemClick)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ImageListItem(
    image: PixabayResponse.PixabayImage,
    onItemClick: (PixabayResponse.PixabayImage) -> Unit
) {
    Card(
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth(),
        elevation = 4.dp,
        onClick = { onItemClick(image) }

    ) {
        Row(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth()
        ) {
            // Display image on the left side
            Image(
                painter = rememberAsyncImagePainter(image.previewURL),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(64.dp)
                    .clip(CircleShape)
                    .border(1.dp, Color.Gray, CircleShape)
            )

            Column(
                modifier = Modifier
                    .padding(start = 16.dp)
                    .wrapContentSize()
            ) {
                // Display user name at the top-right corner
                Text(
                    text = image.user,
                    style = MaterialTheme.typography.h6,
                    fontWeight = FontWeight.Bold
                )

                // Display tags at the bottom-right corner
                Text(
                    text = image.tags,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .align(Alignment.End)
                        .padding(top = 8.dp)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    val image = PixabayResponse.PixabayImage(
        id = 2310212,
        tags = "fruit, lemon, art",
        previewURL = "https://cdn.pixabay.com/photo/2017/05/13/17/31/fruit-2310212_150.jpg",
        largeImageURL = "https://pixabay.com/get/g1af53fe4006051a0ad74edcaa5eb5b7df21c493b1c04b0c1f9f3278d19eea19668edbd9762c5db61c08af24239abab060d2468f38e6ad023437640f8bba99a3f_1280.jpg",
        downloads = 122928,
        likes = 1064,
        comments = 178,
        user = "Yousz",
        query = "lemon"
    )
    ImageListItem(image, onItemClick = {})
}


