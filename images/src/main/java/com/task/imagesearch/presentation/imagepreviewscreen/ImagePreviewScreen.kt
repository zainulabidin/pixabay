package com.task.imagesearch.presentation.imagepreviewscreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.imagesearch.presentation.components.AppToolbar


@Composable
fun ImagePreviewScreen(pixabayImage: PixabayResponse.PixabayImage?) {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        AppToolbar(
            title = "Image Preview",
        )

        pixabayImage?.let { ImageDetailScreen(it) }

    }
}

@Composable
fun ImageDetailScreen(image: PixabayResponse.PixabayImage) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        // Display bigger version of the image
        Image(
            painter = rememberAsyncImagePainter(image.largeImageURL),
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .fillMaxWidth()
                .height(240.dp)
                .clip(MaterialTheme.shapes.medium)
        )

        Spacer(modifier = Modifier.height(16.dp))

        // Display user name
        Text(
            text = image.user,
            style = MaterialTheme.typography.h5,
            fontWeight = FontWeight.Bold
        )

        // Display tags
        Text(
            text = image.tags,
            style = MaterialTheme.typography.subtitle1,
            color = MaterialTheme.colors.secondary,
            modifier = Modifier.padding(top = 4.dp)
        )

        Spacer(modifier = Modifier.height(8.dp))

        // Display additional details
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column {
                Text(
                    text = "${image.likes} Likes",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.secondaryVariant
                )
                Text(
                    text = "${image.downloads} Downloads",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.secondaryVariant
                )
                Text(
                    text = "${image.comments} Comments",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.secondaryVariant
                )
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun Preview() {
    val image = PixabayResponse.PixabayImage(
        id = 2310212,
        tags = "fruit, lemon, art",
        previewURL = "https://cdn.pixabay.com/photo/2017/05/13/17/31/fruit-2310212_150.jpg",
        largeImageURL = "https://pixabay.com/get/g1af53fe4006051a0ad74edcaa5eb5b7df21c493b1c04b0c1f9f3278d19eea19668edbd9762c5db61c08af24239abab060d2468f38e6ad023437640f8bba99a3f_1280.jpg",
        downloads = 122928,
        likes = 1064,
        comments = 178,
        user = "Yousz",
        query = "lemon"
    )
    ImageDetailScreen(image)
}



