package com.task.imagesearch.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.task.core.ui.RequestStates
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.imagesearch.domain.usecase.SearchImageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchImagesViewModel @Inject constructor(
    private val searchImageUseCase: SearchImageUseCase
) : ViewModel() {

    private var searchJob: Job? = null

    private val _ImagesState = MutableStateFlow<RequestStates<List<PixabayResponse.PixabayImage>>>(RequestStates.Loading)
    val imagesState: StateFlow<RequestStates<List<PixabayResponse.PixabayImage>>> = _ImagesState

    fun getImages(query: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            delay(1000)
            searchImageUseCase.invoke(query).collect { state ->
                _ImagesState.value = state
            }
        }
    }

}