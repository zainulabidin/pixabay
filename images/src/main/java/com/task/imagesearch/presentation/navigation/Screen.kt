package com.task.imagesearch.presentation.navigation

sealed class Screen(val route: String){
    object ImagesListScreen: Screen("image_list_screen")
    object ImagePreviewScreen: Screen("image_preview_screen")
}