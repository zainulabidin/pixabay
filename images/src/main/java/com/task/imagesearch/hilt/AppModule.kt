package com.task.imagesearch.hilt

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.task.core.BuildConfig
import com.task.core.R
import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.data.local.AppDatabase
import com.task.imagesearch.data.local.ImagesLocalDataSource
import com.task.imagesearch.data.local.dao.ImagesDao
import com.task.imagesearch.data.remote.ImagesRemoteDataSource
import com.task.imagesearch.data.remote.retrofit.Endpoints
import com.task.imagesearch.data.repository.SearchImageRepository
import com.task.imagesearch.domain.usecase.SearchImageUseCase
import com.task.util.NetworkHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()


    @Singleton
    @Provides
    fun provideRetrofit(@ApplicationContext context: Context, gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.pixabay_api_base_url))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()


    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideApiService(retrofit: Retrofit): Endpoints = retrofit.create(Endpoints::class.java)

    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "app_database"
        ).build()
    }

    @Provides
    fun provideDao(appDatabase: AppDatabase): ImagesDao {
        return appDatabase.imagesDao
    }

    @Provides
    @Singleton
    fun provideSearchImageUseCase(repository: SearchImageRepository): SearchImageUseCase {
        return SearchImageUseCase(repository)
    }

    @Provides
    @Singleton
    fun provideSearchImageRepository(
        remoteDataSource: SearchImagesDataSource.Remote,
        localDataSource: SearchImagesDataSource.Local,
        networkHelper: NetworkHelper
    ): SearchImageRepository {
        return SearchImageRepository(remoteDataSource, localDataSource, networkHelper)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(endpoints: Endpoints): SearchImagesDataSource.Remote {
        return ImagesRemoteDataSource(endpoints)
    }

    @Provides
    @Singleton
    fun provideLocalDataSource(dao: ImagesDao): SearchImagesDataSource.Local {
        return ImagesLocalDataSource(dao)
    }

    @Provides
    @Singleton
    fun provideNetworkUtils(@ApplicationContext context: Context): NetworkHelper {
        return NetworkHelper(context)
    }
}
