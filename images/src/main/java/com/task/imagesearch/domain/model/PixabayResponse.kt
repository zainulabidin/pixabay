package com.task.imagesearch.domain.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

data class PixabayResponse(
    val total: Int,
    val totalHits: Int,
    val hits: List<PixabayImage>
){
    @Parcelize
    @Entity(tableName = "images")
    data class PixabayImage(
        @PrimaryKey
        val id: Int,
        val tags: String,
        val previewURL: String,
        val largeImageURL: String,
        val downloads: Int,
        val likes: Int,
        val comments: Int,
        val user: String,
        val query: String // Store the associated query
    ): Parcelable
}


