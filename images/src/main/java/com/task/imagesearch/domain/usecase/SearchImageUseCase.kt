package com.task.imagesearch.domain.usecase

import com.task.core.ui.RequestStates
import com.task.imagesearch.data.repository.SearchImageRepository
import com.task.imagesearch.domain.model.PixabayResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList

class SearchImageUseCase(private val repository: SearchImageRepository) {

    operator fun invoke(query: String): Flow<RequestStates<List<PixabayResponse.PixabayImage>>> {
        return flow {
            val requestStates = repository.getImages(query)
            val imagesState = requestStates.toList().lastOrNull()

            if (imagesState is RequestStates.Success && imagesState.data.isEmpty()) {
                emit(RequestStates.Error(Exception("No Image Found")))
            } else {
                emitAll(requestStates)
            }
        }
    }
}
