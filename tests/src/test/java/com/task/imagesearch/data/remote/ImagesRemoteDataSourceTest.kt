package com.task.imagesearch.data.remote

import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.data.remote.retrofit.Endpoints
import com.task.imagesearch.domain.model.PixabayResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ImagesRemoteDataSourceTest {

    @Mock
    private lateinit var mockEndpoints: Endpoints

    private lateinit var remoteDataSource: SearchImagesDataSource.Remote

    @Before
    fun setUp() {
        remoteDataSource = ImagesRemoteDataSource(mockEndpoints)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testGetImagesApiResponseSuccess() = runTest {
        val query = "fruit"
        val mockResponse = PixabayResponse(
            total = 100,
            totalHits = 200,
            hits = mockImageList
        )

        `when`(mockEndpoints.getPixabayResponse(query = query)).thenReturn(mockResponse)

        val result = remoteDataSource.getImagesApiResponse(query)

        assertThat(result).isEqualTo(mockResponse)
    }

    val mockImageList = listOf(
        PixabayResponse.PixabayImage(
            id = 2305192,
            tags = "fresh fruits, bowls, fruit bowls",
            previewURL = "https://cdn.pixabay.com/photo/2017/05/11/19/44/fresh-fruits-2305192_150.jpg",
            largeImageURL = "https://pixabay.com/get/g178524bdf1f23885663b92327efc22c1b39ec130a1e38389f3945a7d15837a6ca71ba7ae55f0af32f5b9340fa3936a96a27a59bd7488523fa604ad2bb895371b_1280.jpg",
            downloads = 296966,
            likes = 1185,
            comments = 234,
            user = "silviarita",
            query = ""
        )
    )
}
