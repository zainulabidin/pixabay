package com.task.imagesearch.data.repository

import com.task.core.ui.RequestStates
import com.task.imagesearch.data.SearchImagesDataSource
import com.task.imagesearch.domain.model.PixabayResponse
import com.task.util.NetworkHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchImageRepositoryTest {

    @Mock
    private lateinit var mockRemoteDataSource: SearchImagesDataSource.Remote

    @Mock
    private lateinit var mockLocalDataSource: SearchImagesDataSource.Local

    @Mock
    private lateinit var mockNetworkHelper: NetworkHelper

    private lateinit var repository: SearchImageRepository

    @Before
    fun setUp() {
        repository =
            SearchImageRepository(mockRemoteDataSource, mockLocalDataSource, mockNetworkHelper)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testGetImagesSuccess_LocalOnly() = runTest {
        val query = "fruits"
        val localResults = mockImageList
        `when`(mockLocalDataSource.getImages(query)).thenReturn(flowOf(localResults))
        `when`(mockNetworkHelper.isNetworkConnected()).thenReturn(false)

        val result = repository.getImages(query).toList()

        assertThat(result).isEqualTo(
            listOf(
                RequestStates.Loading,
                RequestStates.Success(localResults)
            )
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testGetImagesSuccess_RemoteAndUpdateLocal() = runTest {
        val query = "fruits"
        val localResults = mockImageList
        val remoteResults = PixabayResponse(
            total = 100,
            totalHits = 200,
            hits = mockImageList
        )

        `when`(mockLocalDataSource.getImages(query)).thenReturn(flowOf(localResults))
        `when`(mockNetworkHelper.isNetworkConnected()).thenReturn(true)
        `when`(mockRemoteDataSource.getImagesApiResponse(query)).thenReturn(remoteResults)
        // Assuming the localDataSource.insertImages function works correctly

        val result = repository.getImages(query).toList()

        val expectedResults = listOf(
            RequestStates.Loading,
            RequestStates.Success(localResults),
            RequestStates.Success(remoteResults.hits.map { it.copy(query = query) })
        )
        assertThat(result).isEqualTo(expectedResults)


    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testGetImagesError_RemoteFails() = runTest {
        val query = "fruits"
        val localResults: List<PixabayResponse.PixabayImage> = mockImageList
        val errorMessage = "Network error"

        `when`(mockLocalDataSource.getImages(query)).thenReturn(flowOf(localResults))
        `when`(mockNetworkHelper.isNetworkConnected()).thenReturn(true)
        `when`(mockRemoteDataSource.getImagesApiResponse(query)).thenThrow(RuntimeException(errorMessage))

        val result = repository.getImages(query).toList()

        val expectedResults = listOf(
            RequestStates.Loading,
            RequestStates.Success(localResults),
            RequestStates.Error(RuntimeException(errorMessage))
        )


        assertThat(result.size).isEqualTo(expectedResults.size)
        assertThat(result.get(0)).isEqualTo(expectedResults.get(0))
        assertThat(result.get(1)).isEqualTo(expectedResults.get(1))
        assertThat(result.get(2).toString()).isEqualTo(expectedResults.get(2).toString())
    }

}

val mockImageList = listOf(
    PixabayResponse.PixabayImage(
        id = 2305192,
        tags = "fresh fruits, bowls, fruit bowls",
        previewURL = "https://cdn.pixabay.com/photo/2017/05/11/19/44/fresh-fruits-2305192_150.jpg",
        largeImageURL = "https://pixabay.com/get/g178524bdf1f23885663b92327efc22c1b39ec130a1e38389f3945a7d15837a6ca71ba7ae55f0af32f5b9340fa3936a96a27a59bd7488523fa604ad2bb895371b_1280.jpg",
        downloads = 296966,
        likes = 1185,
        comments = 234,
        user = "silviarita",
        query = "fruits"
    )
)
